
const users = [
	{
		 uid:001
		,email:'john@dev.com'
		,personalInfo:{
			name:'John'
		   ,address:{
				line1:'westwish st'
			   ,line2:'washmasher'
			   ,city:'wallas'
			   ,state:'WX'
			}
		}
	},
	{
		 uid:063
		,email:'a.bken@larobe.edu.au'
		,personalInfo:{
			name:'amin'
		   ,address:{
				line1:'Hedelberg'
			   ,line2:''
			   ,city:'Melbourne'
			   ,state:'VIC'
			}
		}
	},
	{
		 uid:045
		,email:'Linda.Paterson@gmail.com'
		,personalInfo:{
			name:'Linda'
		   ,address:{
				line1:'Cherry st'
			   ,line2:'kangaroo Point'
			   ,city:'Brisbane'
			   ,state:'QLD'
			}
		}
	}

];

function mapJson_Table(json){
	let tHead = buildTHead(json);
	
	let tBody = buildTBody(json);
	
	//build the table
  const table = `
	<table>
		<thead>
			<tr>${tHead}</tr>
		<thead>
		<tbody>
			${tBody}
		<tbody>
	<table>`;

  return table;
	
}
/*
	build tHead data, delegating call to json_keys
*/
function buildTHead(json){
	let headers=[];
	headers = json_keys(json[0]);
	
	let headerRow = headers
					.map(head => `<th>${head}</th>`)
					.join("");
	return headerRow;
}
/*
	build tBody data, delegating calls to various methods
*/
function buildTBody(json){
	
	let cols = Object.keys(json[0]);
	let rows = json
    .map(row => {
      let tds = cols
				.map(col => buildRow(row,col)).join("");
      return `<tr>${tds}</tr>`;
    })
    .join("");
	
	return rows;
}
/*
	filters json to get nested keys
*/
function json_keys(input)
{
   let allKeys = [];
    for (key in input)
    {
		if(typeof input[key] == 'object'){
			Object.keys(input[key])
				  .forEach(nKey => allKeys.push(nKey));
			continue;
		}
		allKeys.push(key);
    }
	return allKeys;
}
/*
	filters row on basis of personalInfo | not
*/
function buildRow(row,head){
	
	if(typeof row[head] != 'object')
		return `<td>${row[head]}</td>`;
	
	return buildPersonalInfo(row,head);
}
/*
	speacial method for handling personalInfo mapping->table
*/
function buildPersonalInfo(row,head){
	
	let name = ""
	let address = ""
	Object.keys(row[head])
		  .forEach(pI => {
			  if(typeof row[head][pI] == 'object'){
				Object.keys(row[head][pI])
					  .forEach(addr => {
          
						  address += `<b>${addr}</b>` 
								  + " : " 
							      + row[head][pI][addr]
								  +"\t";
					  })
			  }else{
				  name = row[head][pI]
			  }
			  
		});
	
	return `<td>${name}</td>
			<td>${address}</td>`;
}
const filterInput = [{name:'John',email:'john@dev.com',state:'WX'},
			 {name:'amin',email:'a.bken@larobe.edu.au',state:'VIC'},
			 {name:'Linda',email:'Linda.Paterson@gmail.com',state:'QL D'}];

let filteredData = [];

function sortByName(nameOrder){
	
	let orderedData = _.orderBy(filteredData, ['personalInfo.name'],[nameOrder]);
	
	output = document.getElementById('task6');
	output.innerHTML = mapJson_Table(orderedData);
}


function returnUsers(filterData){
	
	filteredData =
	users.filter(
		(set => 
		user => set.has(user.email))
				(new Set(filterData.map
									(user => user.email))));
	
	return filteredData;
}

function populateTable(){	
output = document.getElementById('task6');
output.style.display = 'none';
let input = returnUsers(filterInput);
output.innerHTML = mapJson_Table(input);
}
populateTable();

