
function toggle(toggleButton){
  let tableId ="task6"; 
  tableToggle(tableId);
  sortButtonToggle(tableId);
  toggleText(toggleButton);
}

function sortButtonToggle(id){
	
	isVisible(id)?
        setOrderButton('inline-block') : setOrderButton('none');
}
function setOrderButton(view){
	
	document.getElementById('orderBtn_asc').style.display = view;
	document.getElementById('orderBtn_desc').style.display = view;
}

function toggleText(btn){
	let text = btn.childNodes[0].innerHTML;
	
	text == "Show Users" ? 
  			setUserButton(btn,"Hide Users") : setUserButton(btn,"Show Users");
	
}

function setUserButton(btn,buttonText){
	btn.childNodes[0].innerHTML = buttonText;
}

function tableToggle(id){
   
  isVisible(id)?
        setVisibiltyFalse(id) : setVisibiltyTrue(id);
}

function isVisible(id){
  return document.getElementById(id).style.display!='none';
} 
function setVisibiltyTrue(id){
  return document.getElementById(id).style.display='inline-block';
}
function setVisibiltyFalse(id){
  return document.getElementById(id).style.display='none';
}